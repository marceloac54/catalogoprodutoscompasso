## Download dependências, build e test da aplicação
- mvn clean install

## Test
- mvn test

## Rodar applicação
- mvn spring-boot:start

## Swagger UI
- http://localhost:9090/swagger-ui.html
