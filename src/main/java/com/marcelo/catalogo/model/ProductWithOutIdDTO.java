package com.marcelo.catalogo.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductWithOutIdDTO {
	

	@NotNull(message = "Nome é obrigatório")
	String name;
	@NotNull(message = "Descrição é obrigatório")
	String description;
	@NotNull(message = "Preço é obrigatório")
	@Min(value = 0L, message = "Preço não pode ser valor negativo")
	BigDecimal price;

}
