package com.marcelo.catalogo.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "products")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	
	@Id
	@Column(name = "id")
	String id;
	@Column(name = "name")
	String name;
	@Column(name = "description")
	String description;
	@Column(name = "price")
	BigDecimal price;

}
