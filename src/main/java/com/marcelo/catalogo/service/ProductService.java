package com.marcelo.catalogo.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.marcelo.catalogo.exception.EntityNotFoundCatalogException;
import com.marcelo.catalogo.model.Product;
import com.marcelo.catalogo.model.ProductDTO;
import com.marcelo.catalogo.model.ProductWithOutIdDTO;
import com.marcelo.catalogo.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository prodRepository;
	
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Transactional(readOnly = true)
	public List<Product> findAllProducts() {
		return prodRepository.findAll();
	}
	
	@Transactional
	public ProductDTO saveProduct(ProductWithOutIdDTO productWithoutIdDto) {
		Product product = modelMapper.map(productWithoutIdDto, Product.class);
		List<Product> list = prodRepository.findByOrderByIdDesc();
		if (list.size() > 0) {
			BigDecimal nextId = new BigDecimal(list.get(0).getId());
			nextId = nextId.add(BigDecimal.ONE);
			String nextIdStr = nextId.toString();
			product.setId(nextIdStr);
		} else {
			product.setId(BigDecimal.ONE.toString());
		}
		product = prodRepository.saveAndFlush(product);
		
		return modelMapper.map(product, ProductDTO.class);
	}

	@Transactional
	public void updateProduct(String productId, ProductDTO productDto) {
		Optional<Product> prod = prodRepository.findById(productId);
		if (prod.isPresent()) {
			modelMapper.map(productDto, prod.get());
			prodRepository.save(prod.get());
			
		} else {
			throw new EntityNotFoundCatalogException("Não foi encontrado produto para respectivo ID");
		}
		
	}

	@Transactional
	public void deleteProduct(String productId) {
		Optional<Product> productToDelete = prodRepository.findById(productId);
		if (productToDelete.isPresent()) {
			prodRepository.delete(productToDelete.get());	
		}
		throw new EntityNotFoundCatalogException("Não foi encontrado produto para respectivo ID");
	}

	@Transactional(readOnly = true)
	public List<ProductDTO> search(BigDecimal minPrice, BigDecimal maxPrice, String query) {
		return prodRepository.filterProduct(minPrice, maxPrice, query);
	}

	@Transactional(readOnly = true)
	public ProductDTO getProductById(String productId) {
		Optional<Product> prod = prodRepository.findById(productId);
		if (!prod.isPresent()) {
			throw new EntityNotFoundCatalogException("Não foi encontrado produto para respectivo ID");
		}
		
		return modelMapper.map(prod.get(), ProductDTO.class);
		
	}
	
}
