package com.marcelo.catalogo.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.catalogo.model.Product;
import com.marcelo.catalogo.model.ProductDTO;
import com.marcelo.catalogo.model.ProductWithOutIdDTO;
import com.marcelo.catalogo.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/products")
@Api(value = "API REST CATÁLOGO DE PRODUTOS")
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping
	@ApiOperation(value = "Retorna uma lista de produtos")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Cidade atualizada")
	})
	public List<Product> listAll() {
		return productService.findAllProducts();
	}
	
	@PostMapping
	@ApiOperation(value = "Salva um produto")
	@ApiResponses({
		@ApiResponse(code = 201, message = "Produto criado com sucesso"),
		@ApiResponse(code = 400, message = "Parâmetros inválidos")
	})
	public ResponseEntity<ProductDTO> saveProduct(@RequestBody @Valid ProductWithOutIdDTO productWithoutIdDTO) {
		ProductDTO productSavedDto = productService.saveProduct(productWithoutIdDTO);
		return new ResponseEntity<>(productSavedDto, HttpStatus.CREATED);
	}
	
	@PutMapping("{id}")
	@ApiOperation(value = "Atualiza um produto")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Produto atualizado com sucesso"),
		@ApiResponse(code = 404, message = "ID não encontrado"),
		@ApiResponse(code = 400, message = "Parâmetros inválidos")
	})
	public void updateProduct(@PathVariable("id") String productId, @RequestBody @Valid ProductDTO productDto) {
		productService.updateProduct(productId, productDto);
	}
	
	@GetMapping("{id}")
	@ApiOperation(value = "Retorna um produto")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Produto retornado com sucesso"),
		@ApiResponse(code = 404, message = "ID não encontrado")
	})
	public ProductDTO getProductById(@PathVariable("id") String productId) {
		return productService.getProductById(productId);
	}
	
	@GetMapping("search")
	@ApiOperation(value = "Busca produtos considerando os parâmetros da url")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Lista de produtos retornada com sucesso")
	})
	public List<ProductDTO> searchProduct(@RequestParam("min_price") @Nullable BigDecimal minPrice,
				@RequestParam("max_price") @Nullable BigDecimal maxPrice, @RequestParam("q") @Nullable String query) {
		System.out.println("oi min_price: "+minPrice);
		System.out.println("oi max_price: "+maxPrice);
		System.out.println("oi q: "+query);
		return productService.search(minPrice, maxPrice, query);
	}
	
	@DeleteMapping("{id}")
	@ApiOperation(value = "Remove um produto")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Produto removido com sucesso"),
		@ApiResponse(code = 404, message = "ID não encontrado")
	})
	public void deleteProduct(@PathVariable("id") String productId) {
		productService.deleteProduct(productId);
	}
}
