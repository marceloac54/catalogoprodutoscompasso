package com.marcelo.catalogo.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.marcelo.catalogo.model.ProductDTO;

public class ProductRepositoryImpl implements ProductQueries{

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<ProductDTO> filterProduct(BigDecimal minPrice, BigDecimal maxPrice, String query) {
		System.out.println("oi filter product");
		
		String jpql = "select new com.marcelo.catalogo.model.ProductDTO("
				+ "id, name, description, price) from Product where 1=1 ";
				if (minPrice != null) {
					jpql= jpql	+ " and (price >= "+minPrice+") ";
				}
				if (maxPrice != null) {
					jpql = jpql	+ " and (price <= "+maxPrice+") ";
				}
				if (query != null) {
					jpql = jpql	+ "and (lower(name) like lower('%"+ query +"%') or lower(description) like ('%"+query+"%'))";
				}
					
		
		List<ProductDTO> result = manager.createQuery(jpql, ProductDTO.class)
//					.setParameter("name", query + "%")
//					.setParameter("minPrice", minPrice)
//					.setParameter("maxPrice", maxPrice)
					.getResultList();
		return result;
	}

}
