package com.marcelo.catalogo.repository;

import java.math.BigDecimal;
import java.util.List;

import com.marcelo.catalogo.model.ProductDTO;

public interface ProductQueries {
	
	List<ProductDTO> filterProduct(BigDecimal minPrice, BigDecimal maxPrice, String query);
}
