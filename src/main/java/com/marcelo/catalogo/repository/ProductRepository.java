package com.marcelo.catalogo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marcelo.catalogo.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String>, ProductQueries{
	
	List<Product> findByOrderByIdDesc();
}
