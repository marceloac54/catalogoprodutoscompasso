package com.marcelo.catalogo.exception;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		StringBuilder b = new StringBuilder();
		ex.getBindingResult().getFieldErrors().stream().map(error -> "campo: "+error.getField()+", mensagem: " + error.getDefaultMessage() +", valor recusado: "+error.getRejectedValue() + ". ")
        	.collect(Collectors.toList()).forEach(b::append);
		
		return new ResponseEntity<>(new ErrorResponseDTO(status.value(),b.toString()),status);
	}
	
	@ExceptionHandler(EntityNotFoundCatalogException.class)
	public ResponseEntity<?> handleNegocio(EntityNotFoundCatalogException ex, WebRequest request) {

		HttpStatus status = HttpStatus.NOT_FOUND;
		
		ErrorResponseDTO errorResponse = new ErrorResponseDTO(status.value(), ex.getMessage());
        return new ResponseEntity<>(errorResponse, status);
	}
}
