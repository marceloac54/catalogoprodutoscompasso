package com.marcelo.catalogo.exception;

public class EntityNotFoundCatalogException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1943151636378098310L;
	
	
	public EntityNotFoundCatalogException(String msg) {
		super(msg);
	}
}
