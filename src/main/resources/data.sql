DROP TABLE IF EXISTS products;
 
CREATE TABLE products (
  id VARCHAR(200)  NOT NULL PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  description VARCHAR(250) NOT NULL,
  price DOUBLE NOT NULL
);
	
--INSERT INTO products VALUES ('1', 'name 1', 'descricao produto ', 12.32); 
--INSERT INTO products VALUES ('2', 'name 2', 'descricao produto 2', 13.22);
--INSERT INTO products VALUES ('3', 'name 3', 'descricao produto 3', 3.32);