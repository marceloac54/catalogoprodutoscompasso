package com.marcelo.catalogo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.marcelo.catalogo.exception.EntityNotFoundCatalogException;
import com.marcelo.catalogo.helper.ProdutoHelper;
import com.marcelo.catalogo.model.Product;
import com.marcelo.catalogo.model.ProductDTO;
import com.marcelo.catalogo.model.ProductWithOutIdDTO;
import com.marcelo.catalogo.repository.ProductRepository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ProductServiceTest {

	
	@Autowired
	private ProductService productService;
	
	@MockBean
	private ModelMapper modelMapper;
	
	@MockBean
	private ProductRepository productRepository;
	
	@Test
	public void shouldSaveProductDefault() {
		
		//context
		ProductWithOutIdDTO productToSave = ProdutoHelper.createProductWithOutIdDTODefault("Produto A", "Descrição do Produto A", new BigDecimal("100.01"));
				
		Product product = ProdutoHelper.createProductDefault();
		when(modelMapper.map(productToSave, Product.class)).thenReturn(product);
		
		when(productRepository.findByOrderByIdDesc())
			.thenReturn(new ArrayList<Product>());
		Product prodWithId1 = product;
		prodWithId1.setId("1");
		when(productRepository.saveAndFlush(product)).thenReturn(prodWithId1);
		when(modelMapper.map(prodWithId1, ProductDTO.class)).thenReturn(ProdutoHelper.createProductDtoDefault());
		
		//action
		ProductDTO savedProduct = productService.saveProduct(productToSave);
		
		//verify
		assertThat(savedProduct.getId(), is("1"));
		assertThat(savedProduct.getName(), equalTo("Produto A"));
		assertThat(savedProduct.getDescription(), equalTo("Descrição do Produto A"));
		assertThat(savedProduct.getPrice(), equalTo(new BigDecimal("100.01")));
		
	}
	
	@Test 
	public void shouldThrowEntityNotFoundCatalogException() {
		//context
		ProductDTO productDto = ProdutoHelper.createProductDtoDefault();
		when(productRepository.findByOrderByIdDesc())
			.thenReturn(new ArrayList<Product>());
		
		try {
			//action
			productService.updateProduct(productDto.getId(), productDto);
			fail("Deveria lançar EntityNotFoundCatalogException");
		} catch (EntityNotFoundCatalogException e) {
			//verify
			assertThat(e.getMessage(), equalTo("Não foi encontrado produto para respectivo ID"));
		}
		
		
	}
	
	@Test
	public void shouldThrowEntityNotFoundCatalogException_WhenGetProductById() {
		//context
		when(productRepository.findById("1"))
			.thenReturn(Optional.empty());
		
		try {
			//action
			productService.getProductById("1");
			fail("Deveria lançar EntityNotFoundCatalogException");
		} catch (EntityNotFoundCatalogException e) {
			//verify
			assertThat(e.getMessage(), equalTo("Não foi encontrado produto para respectivo ID"));
		}
		
	}
	
	@Test
	public void ShouldThrowEntityNotFoundCatalogException_WhenDeleteProduct() {
		//context
		when(productRepository.findById("1")).thenReturn(Optional.empty());
		
		try {
			//action
			productService.deleteProduct("1");
			fail("Deveria lançar EntityNotFoundCatalogException");
		} catch (EntityNotFoundCatalogException e) {
			//verify
			assertThat(e.getMessage(), equalTo("Não foi encontrado produto para respectivo ID"));
		}
		
	}

}
