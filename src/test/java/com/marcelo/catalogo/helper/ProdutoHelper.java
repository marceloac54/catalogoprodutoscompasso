package com.marcelo.catalogo.helper;

import java.math.BigDecimal;

import com.marcelo.catalogo.model.Product;
import com.marcelo.catalogo.model.ProductDTO;
import com.marcelo.catalogo.model.ProductWithOutIdDTO;

public class ProdutoHelper {
	
	
	public static ProductWithOutIdDTO createProductWithOutIdDTODefault(String name, String descricao, BigDecimal preco) {
		return ProductWithOutIdDTO.builder()
				.name(name)
				.description(descricao)
				.price(preco)
				.build();
	}
	
	public static ProductDTO createProductDtoDefault() {
		return ProductDTO.builder()
				.id("1")
				.name("Produto A")
				.description("Descrição do Produto A")
				.price(new BigDecimal("100.01")).build();
	}

	public static Product createProductDefault() {
		return Product.builder()
				
				.name("Produto A")
				.description("Descrição do Produto A")
				.price(new BigDecimal("100.01")).build();
	}

}
