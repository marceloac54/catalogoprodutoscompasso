package com.marcelo.catalogo.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marcelo.catalogo.helper.ProdutoHelper;
import com.marcelo.catalogo.model.ProductDTO;
import com.marcelo.catalogo.model.ProductWithOutIdDTO;
import com.marcelo.catalogo.repository.ProductRepository;
import com.marcelo.catalogo.service.ProductService;


@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {
	
	public static final String PRODUCT_ENDPOINT = "/products";
	
	@Autowired
    private MockMvc mvc;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductService productService;
	
	@BeforeEach
	public void setup() {
		productRepository.deleteAll();
	}
	
	@Test
	public void shouldGet2Products() throws Exception {
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome a", "decricao a", new BigDecimal("1.24")));
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome b", "decricao b", new BigDecimal("1232.232")));
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.get("/products");
		
		mvc
		.perform(request)
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(2)));
	}
	
	@Test
	public void shouldSaveProduct() throws Exception {
		ProductWithOutIdDTO product = ProdutoHelper.createProductWithOutIdDTODefault("nome a"
				, "decricao a", new BigDecimal("12"));
		
		String json = new ObjectMapper().writeValueAsString(product);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.post(PRODUCT_ENDPOINT)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(json);
		
		mvc
		.perform(request)
		.andExpect(status().is2xxSuccessful())
		.andExpect(jsonPath("id").value(equalTo("1")))
		.andExpect(jsonPath("name").value(equalTo(product.getName())))
		.andExpect(jsonPath("description").value(equalTo(product.getDescription())))
		.andExpect(jsonPath("price").value((product.getPrice().toString())));
	}
	
	@Test
	public void shoulThrowMethodArgumentNotValidException_WhenSaveProductWithNullValues() throws Exception {
		ProductWithOutIdDTO product = new ProductWithOutIdDTO();
		
		String json = new ObjectMapper().writeValueAsString(product);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.post(PRODUCT_ENDPOINT)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(json);
		
		mvc
		.perform(request)
		.andExpect(status().is4xxClientError())
		.andExpect(jsonPath("message")
				.value(("campo: name, mensagem: Nome é obrigatório, valor recusado: null. campo: description, mensagem: Descrição é obrigatório, valor recusado: null. campo: price, mensagem: Preço é obrigatório, valor recusado: null. ")));
	}
	
	@Test
	public void shouldThrowMsgErroUpdateProduct() throws Exception {
		ProductDTO product = ProdutoHelper.createProductDtoDefault();
		
		String json = new ObjectMapper().writeValueAsString(product);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.put(PRODUCT_ENDPOINT+"/2")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(json);
		
		mvc
		.perform(request)
		.andExpect(status().is4xxClientError())
		.andExpect(jsonPath("message")
				.value(equalTo("Não foi encontrado produto para respectivo ID")));
	}
	
	@Test
	public void shouldThowEntityNotFoundCatalog_WhenCallGetProductById() throws Exception {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.get(PRODUCT_ENDPOINT+"/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		
		mvc
		.perform(request)
		.andExpect(status().is4xxClientError())
		.andExpect(jsonPath("message")
				.value(equalTo("Não foi encontrado produto para respectivo ID")));
	}
	
	@Test
	public void shouldReturnProduct_WhenCallGetProductById() throws Exception {
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome a", "decricao a", new BigDecimal("1.24")));
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.get(PRODUCT_ENDPOINT+"/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		
		mvc
		.perform(request)
		.andExpect(status().is2xxSuccessful())
		.andExpect(jsonPath("name").value(equalTo("nome a")))
		.andExpect(jsonPath("description").value(equalTo("decricao a")))
		.andExpect(jsonPath("price").value((new BigDecimal("1.24").toString())));
	}
	
	@Test
	public void shouldSearchProductsByRequestParams_WhenCallSearchProduct() throws Exception {
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome a", "descricao a", new BigDecimal("1.24")));
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome b", "descricao b", new BigDecimal("4")));
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome c", "descricao c", new BigDecimal("5")));
		productService.saveProduct(ProdutoHelper
				.createProductWithOutIdDTODefault("nome d", "descricao d", new BigDecimal("6")));
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.get(PRODUCT_ENDPOINT+"/search?min_price=3&max_price=13&q=desc")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		mvc
		.perform(request)
		.andExpect(status().is2xxSuccessful())
		.andExpect(jsonPath("$", hasSize(3)));
	}
	
}
